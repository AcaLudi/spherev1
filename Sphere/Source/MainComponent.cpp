/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"
#include <sstream>

#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif
#include "geometry.h"
//==============================================================================
MainComponent::MainComponent()
{
    // Make sure you set the size of the component after
    // you add any child components.
    setSize (400, 400);

 
    openGLContext.setRenderer( this );
    openGLContext.setComponentPaintingEnabled( true );
    openGLContext.attachTo( *this );
	//It seems that this doesn't work, though it would have been great if it does, also redraw request is commented out at the end of mouseDrag method
	/*
    #ifndef JUCE_MAC
        openGLContext.setContinuousRepainting( false );
    #else
        openGLContext.setContinuousRepainting( true );
    #endif
    /**/
	openGLContext.setContinuousRepainting(true);
	this->addKeyListener((juce::KeyListener*) this);

	points.reserve(1000);
}

MainComponent::~MainComponent()
{
    // This shuts down the GL system and stops the rendering calls.
}

void MainComponent::initLights()
{
    // set up light colors (ambient, diffuse, specular)
    GLfloat lightKa[] = {.3f, .3f, .3f, 0.1f};  // ambient light
    GLfloat lightKd[] = {0.7f, 0.7f, 0.7f, 0.1f};  // diffuse light
//    GLfloat lightKs[] = {1, 1, 1, 0.5};           // specular light
//    glLightfv(GL_LIGHT0, GL_AMBIENT, lightKa);
      glLightfv(GL_LIGHT0, GL_DIFFUSE, lightKd);
//    glLightfv(GL_LIGHT0, GL_SPECULAR, lightKs);

    // position the light
    float lightPos[4] = {0, 0, -1, 0}; // directional light
    glLightfv(GL_LIGHT0, GL_POSITION, lightPos);

	glLightfv(GL_LIGHT1, GL_AMBIENT, lightKa);

	glEnable(GL_LIGHT0);                        // MUST enable each light source after configuration
	glEnable(GL_LIGHT1);                        // MUST enable each light source after configuration
}

//==============================================================================
void MainComponent::newOpenGLContextCreated()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_CULL_FACE);

    glClearColor(255, 0, 0, 0);                   // background color
    glClearStencil(0);                          // clear stencil buffer
    glClearDepth(1.0f);                         // 0 is near, 1 is far
    glDepthFunc(GL_LEQUAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    initLights();
	mSphere.createElements();
}

void MainComponent::openGLContextClosing()
{
}

void MainComponent::renderOpenGL()
{
	if (!mSphere.elements_ready)
		return;

	const int points_size = (int)points.size();

	for (int i = 0; i < points_size; i++)
	{
		glMouseDrag(points[i]);
		delete[] points[i];
	}


	if (points.size() > 0)
	{
		points.erase(points.begin(), points.begin() + points_size);
	}







	if (current_size[0] > current_size[1])
	{
		viewport[0] = (current_size[0] - current_size[1]) / 2;
		viewport[1] = 0;
		viewport[2] = current_size[1];
		viewport[3] = current_size[1];
	}
	else
	{
		viewport[0] = 0; 
		viewport[1] = (current_size[1] - current_size[0]) / 2;
		viewport[2] = current_size[0];
		viewport[3] = current_size[0];

	}
	glViewport(viewport[0], viewport[1], viewport[2], viewport[3]);



	glClearColor (0.05f, 0.1f, 0.12f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadMatrixf(mSphere.matrix);
	geometry::invertM(mSphere.matrix_inv, mSphere.matrix);
	
    float lineColor[] = {0.2f, 0.2f, 0.2f, 1};

    //glRotatef(-90, 1, 0, 0);
    //mSphere.drawWithLines(lineColor);
	float * mat = mSphere.matrix;
	float K = 180.0f / M_PI;
	/**/
	

	mSphere.calculateEulerRotation();

	/**/
	std::ostringstream buf;
	for(int i = 0;i<4;i++)
		buf << mat[i*4+0] << ", " << mat[i * 4 + 1] << ", " << mat[i * 4 + 2] << ", " << mat[i * 4 + 3] << "\n";

	
	buf << "Rotations Are " << mSphere.roll * K << ", " << mSphere.pitch * K << ", " << mSphere.yaw * K <<  "\n";
	buf << "Left Rotations Are " << mSphere.lr_pos[0] * K << ", " << mSphere.lr_pos[1] * K << "\n";
	buf << "Right Rotations Are " << mSphere.lr_pos[2] * K << ", " << mSphere.lr_pos[3] * K << "\n";


	DBG(buf.str());
	/**/
	//mSphere.setLPos(mSphere.lr_pos[0], mSphere.lr_pos[1]);
	//mSphere.setRPos(mSphere.lr_pos[2], mSphere.lr_pos[3]);
	mSphere.setRotation(mSphere.roll, mSphere.pitch, mSphere.yaw);
	//mSphere.setRotation(a, 0.0f, 0.0f);

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	glDisable(GL_BLEND);
	glCullFace(GL_BACK);



	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	glEnable(GL_CULL_FACE);
	glDisable(GL_BLEND);


	mSphere.drawAxisLines();//let's draw back side of axis marks first





	//glDisable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);


	for (int i = 0; i < 4; i++)
		mSphere.spr_side_marks[i]->draw();

	for (int i = 0; i < 6; i++)
		mSphere.spr_point[i]->draw();

	for (int i = 0; i < 2; i++)
	{
		if(mSphere.spr_hands[i] != nullptr)
			mSphere.spr_hands[i]->draw();
	}
		


	glDisable(GL_CULL_FACE);
	glColor4f(1.0f, 1.00f, 1.0f, 1.0f);
	mSphere.spr_origin->draw();



	glLoadMatrixf(mSphere.matrix_default);
	
	
	
	for (int i = 0; i < 10; i++)
	{
		if (mSphere.spr_arrows[i] != nullptr)
			mSphere.spr_arrows[i]->draw();
		else
			break;
	}
	glDisable(GL_DEPTH_TEST);
	mSphere.spr_middle_point->draw();

	glLoadMatrixf(mSphere.matrix);
	glEnable(GL_DEPTH_TEST);

	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_TEXTURE_2D);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	

	glColor4f(0.4f, 0.5f, 0.55f, 0.3f);//sphere color
	glEnable(GL_BLEND);
	//mSphere.draw();//draw circle that will cover back side and simulate transparence
	glCullFace(GL_FRONT);
	mSphere.draw();//draw circle that will cover back side and simulate transparence
	




	


	glDisable(GL_BLEND);
	mSphere.drawAxisLines();//draw front lines of the axis marks



	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glDisable(GL_DEPTH_TEST);

	for (int i = 0; i < 4; i++)
		mSphere.spr_side_marks[i]->draw();

	for (int i = 0; i < 6; i++)
		mSphere.spr_point[i]->draw();

	for (int i = 0; i < 2; i++)
	{
		if(mSphere.spr_hands[i] != nullptr)
			mSphere.spr_hands[i]->draw();
	}
		
	openGLContext.triggerRepaint();

}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // You can add your component specific drawing code here!
    // This will draw over the top of the openGL background.
}

void MainComponent::resized()
{
	current_size[0] = (float)getWidth();
	current_size[1] = (float)getHeight();

    // This is called when the MainComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
}

void MainComponent::mouseUp(const MouseEvent & event)
{
	mSphere.pointer_locked = -1;
	mouse_button_down = 0;
}

void MainComponent::mouseDown(const MouseEvent & event)
{
	juce::Point<int> point = event.getPosition();
	point.x -= viewport[0];
	point.y -= viewport[1];
	if (point.x < 0 || point.y < 0)
		return;



	float opengl_ray[6]
	{
		-1.0f + ((float)point.x / viewport[2]) * 2.0f,
		+1.0f - ((float)point.y / viewport[3]) * 2.0f,
		-1.0f,

		0.0f,
		0.0f,
		2.0f

	};

	for (int z = 0; z < 2; z++)
		last_pointer_pos[z] = opengl_ray[z];

	float * col_point = mSphere.pointer_locked_point;
	if (geometry::rayToSphereAligned(opengl_ray, mSphere.getRadius(), col_point))
	{
		
		float koef = col_point[0];
		for (int z = 0; z < 3; z++)
			col_point[z] = opengl_ray[z] + opengl_ray[z + 3] * koef;

		/*
		std::ostringstream buf;
		buf << "Collision happens at [" << col_point[0] << ", " << col_point[1] << ", " << col_point[2] << "]";
		DBG(buf.str());
		/**/
		float vec[3];//we ll use this vector to calculate distance to the hand sprites
		float * mat = mSphere.matrix;

		for (int j = 0; j < 2; j++)
		{
			if (mSphere.spr_hands[j] != nullptr)
			{
				float hand_pos[3];

				float * src_pos = mSphere.spr_hands[j]->pos;


				for (int z = 0; z < 3; z++)
					hand_pos[z] = src_pos[0] * mat[z] + src_pos[1] * mat[4 + z] + src_pos[2] * mat[8 + z] + mat[12 + z];



				for (int z = 0; z < 3; z++)
					vec[z] = hand_pos[z] - col_point[z];
				float distance = geometry::vectorNorm(vec);
				if (distance < mSphere.spr_hands[j]->size[0])//if colliding point on sphere is close enough to one of the hands, we ll lock pointer to the hand and move the hand instead of rotating the sphere
				{
					mSphere.pointer_locked = j;
					for (int z = 0; z < 3; z++)
						mSphere.vec_capture[z] = mSphere.spr_hands[j]->pos[z];


					float * mat = mSphere.matrix_inv;
					float tmp[3];
					for (int z = 0; z < 3; z++)
						tmp[z] = col_point[0] * mat[z] + col_point[1] * mat[4 + z] + col_point[2] * mat[8 + z] + mat[12 + z];

					for (int z = 0; z < 3; z++)
						col_point[z] = tmp[z];

					break;

				}
			}


		}

		
		
		
		if (mSphere.pointer_locked < 0)//if hand was not touched, than we ll lock pointer to rotate sphere
		{
			mSphere.pointer_locked = 3;
			for (int z = 0; z < 16; z++)
				mSphere.matrix_capture[z] = mSphere.matrix[z];

		}
			

	}


}


void MainComponent::mouseDrag(const MouseEvent & event)
{

	juce::Point<int> point = event.getPosition();

	point.x -= viewport[0];
	point.y -= viewport[1];
	if (point.x < 0 || point.y < 0)
		return;


	float * point_2 = new float[2];
	point_2[0] = ((float)point.x / viewport[2]);
	point_2[1] = ((float)point.y / viewport[3]);

	points.push_back(point_2);


	if (event.mods.isMiddleButtonDown())
	{
		mouse_button_down = 2;
	}
	else if (event.mods.isLeftButtonDown())
	{
		mouse_button_down = 1;
	}
	else if (event.mods.isRightButtonDown())
	{
		mouse_button_down = 3;
	}




}
void MainComponent::glMouseDrag(float * point)
{


	float opengl_ray[6]
	{
		-1.0f + point[0] * 2.0f,
		+1.0f - point[1] * 2.0f,
		-1.0f,

		0.0f,
		0.0f,
		2.0f

	};
	const int lp = mSphere.pointer_locked;

	if (lp == 3)
	{
		
		float scroll[2];
		for (int z = 0; z < 2; z++)
			scroll[z] = opengl_ray[z] - last_pointer_pos[z];

		float mat[16];

		for (int z = 0; z < 16; z++)
			mat[z] = mSphere.matrix_capture[z];

		if (mouse_button_down == 2)
		{
			float col_point[3];
			if (geometry::rayToSphereAligned(opengl_ray, mSphere.getRadius(), col_point))
			{
				std::ostringstream buf;
				float koef = col_point[0];
				for (int z = 0; z < 3; z++)
					col_point[z] = opengl_ray[z] + opengl_ray[z + 3] * koef;

				geometry::pointMatrixVectorTo(mat, mSphere.pointer_locked_point, col_point);
				for (int z = 0; z < 16; z++)
					mSphere.matrix[z] = mat[z];

				//DBG("Sphere Rotated...");
			}
		}
		else if (mouse_button_down == 1)
		{

#if SPHERE_TYPE==1

			float axis[3];

			axis[0] = mSphere.matrix[0] * mSphere.black_line[0] + mSphere.matrix[4] * mSphere.black_line[1] + mSphere.matrix[8] * mSphere.black_line[2];
			axis[1] = mSphere.matrix[1] * mSphere.black_line[0] + mSphere.matrix[5] * mSphere.black_line[1] + mSphere.matrix[9] * mSphere.black_line[2];
			axis[2] = mSphere.matrix[2] * mSphere.black_line[0] + mSphere.matrix[6] * mSphere.black_line[1] + mSphere.matrix[10] * mSphere.black_line[2];

			geometry::rotateMatrix3D(mat, axis, -scroll[0]);
#else
			geometry::rotateMatrix3D(mat, mSphere.matrix_capture + 4, -scroll[0]);
#endif
			//geometry::rotateMatrix3D(mat, mSphere.matrix_capture + 0, +scroll[1]);





			for (int z = 0; z < 16; z++)
				mSphere.matrix[z] = mat[z];
		}
		else if (mouse_button_down == 3)
		{
			float axis_x[3]{ 1.0f, 0.0f, 0.0f };
			float axis_y[3]{ 0.0f, 1.0f, 0.0f };

			geometry::rotateMatrix3D(mat, axis_y, -scroll[0]);
			geometry::rotateMatrix3D(mat, axis_x, +scroll[1]);


			for (int z = 0; z < 16; z++)
				mSphere.matrix[z] = mat[z];
		}
		

		/*
		
		/**/
	}
	else if(lp > -1)
	{
		
		float col_point[3];
		if (geometry::rayToSphereAligned(opengl_ray, mSphere.getRadius(), col_point))
		{
			std::ostringstream buf;
			float koef = col_point[0];
			for (int z = 0; z < 3; z++)
				col_point[z] = opengl_ray[z] + opengl_ray[z + 3] * koef;

			float vec[3];
			for (int z = 0; z < 3; z++)
				vec[z] = mSphere.vec_capture[z];

			
			float * mat = mSphere.matrix_inv;
		
			float B[3];
			for (int z = 0; z < 3; z++)
				B[z] = col_point[0] * mat[z] + col_point[1] * mat[4 + z] + col_point[2] * mat[8 + z] + mat[12 + z];


			geometry::repointVectorTo(vec, mSphere.pointer_locked_point, B);

			for (int z = 0; z < 3; z++)
				mSphere.spr_hands[lp]->pos[z] = vec[z];

			mSphere.spr_hands[lp]->generateVertices();
	
		}



	}
#ifndef JUCE_MAC
	//renderOpenGL();
#endif

}


bool MainComponent::keyPressed(const juce::KeyPress &key)
{
	std::ostringstream buf;
	buf << "Key pressed [" << key.getKeyCode() << "]";
	
	std::string str = buf.str();
	DBG(str);

	return false;
}
