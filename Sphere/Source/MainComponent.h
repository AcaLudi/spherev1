/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "Sphere.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent : public Component, public OpenGLRenderer
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();


    //==============================================================================
    void paint (Graphics& g) override;
    void resized() override;
	
	bool keyPressed(const juce::KeyPress &key) override;

	//void mouseMove(const MouseEvent & event) override;
	void mouseDown(const MouseEvent & event) override;
	void mouseUp(const MouseEvent & event) override;
	void mouseDrag(const MouseEvent & event) override;



private:

	float last_pointer_pos[2];
	float current_size[2];
	int viewport[4];
    OpenGLContext openGLContext;
    Sphere mSphere;


	vector<float*> points;
	int mouse_button_down = 0;

	void glMouseDrag(float * point);
    void newOpenGLContextCreated() override;
    void renderOpenGL() override;
    void openGLContextClosing() override;
    void initLights();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
};
