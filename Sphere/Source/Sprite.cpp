#include "Sprite.h"

#include "geometry.h"
#include <sstream>

float Sprite::y_axis[3] = { 0.0f, 1.0f, 0.0f };
float Sprite::z_axis[3] = { 0.0f, 0.0f, 1.0f };


Sprite::Sprite(string texture_name, float size_x, float size_y, float pos_x, float pos_y, float pos_z, float _radius)
{
    
    //string workingDir = File::getCurrentWorkingDirectory().getFullPathName();
    //printf("Working dir is [%s].\n", (char*)File::getCurrentWorkingDirectory().getFullPathName().toRawUTF8() );
	File file = File(File::getCurrentWorkingDirectory().getFullPathName() + "/" + texture_name);
	Image im = ImageCache::getFromFile(file);

	
	tex.loadImage(im);
	texture = tex.getTextureID();

	size[0] = size_x * 0.5f;
	size[1] = size_y * 0.5f;


	pos[0] = pos_x;
	pos[1] = pos_y;
	pos[2] = pos_z;

	radius = _radius;

	vertices_count = 4;
	element_type = GL_TRIANGLE_STRIP;
}

void Sprite::generateVertices()
{
	float up[3];
	float right[3];

	geometry::setVectorLength3D(pos, radius);

	if (fabsf(pos[0]) > 0.00001f || fabsf(pos[2]) > 0.00001f)//vector is not vertical
	{
		geometry::multiplyVectorCross(up, pos, y_axis);
	}
	else
	{
		geometry::multiplyVectorCross(up, pos, z_axis);
	}

	geometry::multiplyVectorCross(right, pos, up);


	geometry::setVectorLength3D(up, 1.0f);
	geometry::setVectorLength3D(right, 1.0f);
	if (coords == nullptr)
		coords = new float[12];
	if (normals == nullptr)
		normals = new float[12];

	for (int z = 0; z < 3; z++)
		coords[0 + z] = pos[z] - up[z] * size[1] - right[z] * size[0];

	for (int z = 0; z < 3; z++)
		coords[3 + z] = pos[z] + up[z] * size[1] - right[z] * size[0];

	for (int z = 0; z < 3; z++)
		coords[6 + z] = pos[z] - up[z] * size[1] + right[z] * size[0];

	for (int z = 0; z < 3; z++)
		coords[9 + z] = pos[z] + up[z] * size[1] + right[z] * size[0];
	/*
	std::ostringstream buf;
	buf << "UP is [" << up[0] << ", " << up[1] << ", " << up[2] << "]\n";
	buf << "Right is [" << right[0] << ", " << right[1] << ", " << right[2] << "]\n";

	for (int i = 0; i < 4; i++)
	{
		buf << "Point " << i << "[" << coords[i * 3 + 0] << ", " << coords[i * 3 + 1] << ", " << coords[i * 3 + 2] << "]\n";
	}
	DBG(buf.str());
	/**/

	if (texCoords == nullptr)
	{
		texCoords = new float[8];
		texCoords[0] = 0.0f;
		texCoords[1] = 1.0f;

		texCoords[2] = 1.0f;
		texCoords[3] = 1.0f;

		texCoords[4] = 0.0f;
		texCoords[5] = 0.0f;

		texCoords[6] = 1.0f;
		texCoords[7] = 0.0f;
	}


	float norm[3];
	for (int z = 0; z < 3; z++)
		norm[z] = pos[z];
	geometry::setVectorLength3D(norm, 1.0f);

	for (int z = 0; z < 3; z++)
		normals[0 + z] = norm[z];

	for (int z = 0; z < 3; z++)
		normals[3 + z] = norm[z];

	for (int z = 0; z < 3; z++)
		normals[6 + z] = norm[z];

	for (int z = 0; z < 3; z++)
		normals[9 + z] = norm[z];

	


}

void Sprite::draw()
{

	if (color != nullptr)
		glColor4fv(color);

	glBindTexture(GL_TEXTURE_2D, texture);
    
    
	glVertexPointer(3, GL_FLOAT, 0, coords);
	if(texCoords != nullptr)
		glTexCoordPointer(2, GL_FLOAT, 0, texCoords);
	glNormalPointer(GL_FLOAT, 0, normals);
	glDrawArrays(element_type, 0, vertices_count);


}


Sprite::~Sprite()
{
	if (texCoords != nullptr)
		delete[] texCoords;
	if (coords != nullptr)
		delete[] coords;
	if (normals != nullptr)
		delete[] normals;
	if (color != nullptr)
		delete[] color;

}



Sprite::Sprite(int _vertices_count, float * _coords, float * _normals)
{
	coords = _coords;
	normals = _normals;
	vertices_count = _vertices_count;

}








Sprite * Sprite::createArrow(float point_x, float point_y, float point_z, float r, float g, float b, float a, float radius)
{
	const float wid = radius * 0.0095;
	const float arrow_wid = radius * 0.08f;
	const float arrow_long = radius * 0.15f;
	
	float forw[3]{ point_x, point_y, point_z };
	geometry::setVectorLength3D(forw, 1.0f);

	float right[3];
	geometry::multiplyVectorCross(right, forw, Sprite::y_axis);
	geometry::setVectorLength3D(right, 1.0f);

	float * coords = new float[9 * 3];
	for (int z = 0; z < 3; z++)
		coords[z] = +right[z] * wid;
	for (int z = 0; z < 3; z++)
		coords[3 + z] = -right[z] * wid;
	for (int z = 0; z < 3; z++)
		coords[6 + z] = forw[z] * (radius - arrow_long) - right[z] * wid;


	for (int z = 0; z < 3; z++)
		coords[9 + z] = +right[z] * wid;
	for (int z = 0; z < 3; z++)
		coords[12 + z] = forw[z] * (radius - arrow_long) - right[z] * wid;
	for (int z = 0; z < 3; z++)
		coords[15 + z] = forw[z] * (radius - arrow_long) + right[z] * wid;


	for (int z = 0; z < 3; z++)
		coords[18 + z] = forw[z] * (radius - arrow_long) - right[z] * arrow_wid;
	for (int z = 0; z < 3; z++)
		coords[21 + z] = forw[z] * (radius - arrow_long) + right[z] * arrow_wid;
	for (int z = 0; z < 3; z++)
		coords[24 + z] = forw[z] * (radius);

	float up[3];
	geometry::multiplyVectorCross(up, right, forw);

	float * normals = new float[9 * 3];
	for (int i = 0; i < 9; i++)
	{
		for (int z = 0; z < 3; z++)
		{
			normals[i * 3 + z] = up[z];
		}
	}

	Sprite * spr = new Sprite(9, coords, normals);
	spr->color = new float[4]{ r,g,b,a };
	return spr;


}
