///////////////////////////////////////////////////////////////////////////////
// Sphere.h
// ========
// sphere for OpenGL with (radius, sectors, stacks)
// The min number of sectors is 3 and The min number of stacks are 2.
//
//  AUTHOR: Song Ho Ahn (song.ahn@gmail.com)
// CREATED: 2017-11-01
// UPDATED: 2018-03-23
///////////////////////////////////////////////////////////////////////////////

#ifndef GEOMETRY_SPHERE_H
#define GEOMETRY_SPHERE_H

#define SPHERE_TYPE 1

/*
	SPHERE TYPE
	1 - 2 Vector
	2 - 5 Vector
	3 - 0 Vector
*/



#include <vector>
#include "Sprite.h"
class Sphere
{
public:
    // ctor/dtor
    Sphere(float radius=0.9f, int sectorCount=36, int stackCount=18, bool smooth=true);
	~Sphere();


	float black_line[3];
	float matrix_default[16];
	float matrix_default_inverse[16];
	float matrix_capture[16];
	float vec_capture[3];
	float matrix_inv[16];
	float matrix[16]
	{
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f,
	};
	int pointer_locked = -1;//-1 means nothing, 3-means on drag it should rotate sphere, 0-1 represents the index of the hand it should move
	float pointer_locked_point[3];
	
	float pitch, roll, yaw;
	float lr_pos[4];



	static float axis_x[3];
	static float axis_y[3];
	static float axis_z[3];
	
	Sprite * spr_side_marks[4]	= { nullptr,nullptr,nullptr,nullptr };
	Sprite * spr_point[6]		= { nullptr,nullptr,nullptr,nullptr,nullptr,nullptr };
	Sprite * spr_hands[2]		= { nullptr,nullptr };
	Sprite * spr_origin = nullptr;
	Sprite * spr_middle_point = nullptr;

	Sprite * spr_arrows[10] = { nullptr,nullptr, nullptr,nullptr, nullptr,nullptr, nullptr,nullptr, nullptr,nullptr };

	bool elements_ready = false;


    // getters/setters
    float getRadius() const                 { return radius; }
    int getSectorCount() const              { return sectorCount; }
    int getStackCount() const               { return stackCount; }
    void set(float radius, int sectorCount, int stackCount, bool smooth=true);
    void setRadius(float radius);
    void setSectorCount(int sectorCount);
    void setStackCount(int stackCount);
    void setSmooth(bool smooth);
	void calculateEulerRotation();
	void setRotation(float roll, float pitch, float yaw);


    // for vertex data
    unsigned int getVertexCount() const     { return (unsigned int)vertices.size() / 3; }
    unsigned int getNormalCount() const     { return (unsigned int)normals.size() / 3; }
    unsigned int getTexCoordCount() const   { return (unsigned int)texCoords.size() / 2; }
    unsigned int getIndexCount() const      { return (unsigned int)indices.size(); }
    unsigned int getLineIndexCount() const  { return (unsigned int)lineIndices.size(); }
    unsigned int getTriangleCount() const   { return getIndexCount() / 3; }
    unsigned int getVertexSize() const      { return (unsigned int)vertices.size() * sizeof(float); }
    unsigned int getNormalSize() const      { return (unsigned int)normals.size() * sizeof(float); }
    unsigned int getTexCoordSize() const    { return (unsigned int)texCoords.size() * sizeof(float); }
    unsigned int getIndexSize() const       { return (unsigned int)indices.size() * sizeof(unsigned int); }
    unsigned int getLineIndexSize() const   { return (unsigned int)lineIndices.size() * sizeof(unsigned int); }
    const float* getVertices() const        { return vertices.data(); }
    const float* getNormals() const         { return normals.data(); }
    const float* getTexCoords() const       { return texCoords.data(); }
    const unsigned int* getIndices() const  { return indices.data(); }
    const unsigned int* getLineIndices() const  { return lineIndices.data(); }

    // for interleaved vertices: V/N/T
    unsigned int getInterleavedVertexCount() const  { return getVertexCount(); }    // # of vertices
    unsigned int getInterleavedVertexSize() const   { return (unsigned int)interleavedVertices.size() * sizeof(float); }    // # of bytes
    int getInterleavedStride() const                { return interleavedStride; }   // should be 32 bytes
    const float* getInterleavedVertices() const     { return interleavedVertices.data(); }

    // draw in VertexArray mode
    void draw() const;
    void drawLines(const float lineColor[4]) const;
    void drawWithLines(const float lineColor[4]) const;
	void drawAxisLines();

	void setLPos(float angle_x, float angle_y);
	void setRPos(float angle_x, float angle_y);


    // debug
    void printSelf() const;
	void createElements();
protected:

private:


    // member functions
    void updateRadius();
    void buildVerticesSmooth();
    void buildVerticesFlat();
    void buildInterleavedVertices();
    std::vector<float> computeFaceNormal(float x1, float y1, float z1,
                                         float x2, float y2, float z2,
                                         float x3, float y3, float z3);


	float * generateLineAroundCircle(float width, float * rotation_axis, int divisions_count);
	

    // memeber vars
	
    float radius;
    int sectorCount;                        // longitude, # of slices
    int stackCount;                         // latitude, # of stacks
    bool smooth;
    std::vector<float> vertices;
    std::vector<float> normals;
    std::vector<float> texCoords;
    std::vector<unsigned int> indices;
    std::vector<unsigned int> lineIndices;

    // interleaved
    std::vector<float> interleavedVertices;
    int interleavedStride;                  // # of bytes to hop to the next vertex (should be 32 bytes)

};

#endif
