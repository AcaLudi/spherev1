#pragma once
#include "../JuceLibraryCode/JuceHeader.h"
#include <string>

using namespace std;

class Sprite
{
public:

	OpenGLTexture tex;
	static float y_axis[3];
	static float z_axis[3];
	static Sprite * createArrow(float point_x, float point_y, float point_z, float r, float g, float b, float a, float radius);

	void generateVertices();
	void draw();
	Sprite(int _vertices_count, float * _coords, float * normals);
	Sprite(string texture, float size_x, float size_y, float pos_x, float pos_y, float pos_z, float _radius);
	~Sprite();


	unsigned int texture = 0;
	float size[2];
	float pos[3];
	float radius;

	float * coords		= nullptr;
	float * normals		= nullptr;
	float * texCoords	= nullptr;
	float * color		= nullptr;


	int vertices_count = 0;
	int element_type = GL_TRIANGLES;

};

