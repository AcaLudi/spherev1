#pragma once


#define M_E 2.71828182845904523536
#define M_LOG2E 1.44269504088896340736
#define M_LOG10E 0.434294481903251827651
#define M_LN2 0.693147180559945309417
#define M_LN10 2.30258509299404568402
#define M_PI 3.14159265358979323846
#define M_PI_2 1.57079632679489661923
#define M_PI_4 0.785398163397448309616
#define M_1_PI 0.318309886183790671538
#define M_2_PI 0.636619772367581343076
#define M_1_SQRTPI 0.564189583547756286948
#define M_2_SQRTPI 1.12837916709551257390
#define M_SQRT2 1.41421356237309504880
#define M_SQRT_2 0.707106781186547524401

class geometry
{
public:
	geometry();
	~geometry();



	static void multiplyVectorCross(float * res, float ax, float ay, float az, float bx, float by, float bz);
	static void multiplyVectorCross(float * res, float * a, float * b);
	static bool setVectorLength3D(float * vec, float desired_length);
	static void rotate3DVector(float * v, float angle, float * axis);
	static bool rayToSphereAligned(float * ray, float r, float * res);
	static bool quadricEquation(float a, float b, float c, float * res);
	static void repointVectorTo(float * vec, float * ref_vec, float * look_vec);
	static void pointMatrixVectorTo(float * mat, float * ref_vec, float * look_vec);
	static void rotateMatrix3D(float * m, float * axis, float ang);


	static float vectorNorm(float * a);
	static float multiplyVectorDot(float ax, float ay, float az, float bx, float by, float bz);
	static float angleBetweenVectors(float * a, float * b);
	static bool invertM(float * invOut, float * m);

	static void multiplyMM(float * rm, float * mr, float * ml);


	static void loadIdentity(float * matrix);

	static void rotateM(float * m, float angle, float x, float y, float z);
	static void setRotateM(float * m, float angle, float x, float y, float z);
};

