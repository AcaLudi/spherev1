#include "geometry.h"
#include <cmath>
#include "../JuceLibraryCode/JuceHeader.h"
#include <string>
#include <sstream>


using namespace std;

geometry::geometry()
{
}


geometry::~geometry()
{
}



void geometry::multiplyVectorCross(float * res, float * a, float * b)
{
	res[0] = a[1] * b[2] - a[2] * b[1];
	res[1] = a[2] * b[0] - a[0] * b[2];
	res[2] = a[0] * b[1] - a[1] * b[0];
}

void geometry::multiplyVectorCross(float * res, float ax, float ay, float az, float bx, float by, float bz)
{
	res[0] = ay * bz - az * by;
	res[1] = az * bx - ax * bz;
	res[2] = ax * by - ay * bx;
}



bool geometry::setVectorLength3D(float * vec, float desired_length)
{

	float len = (float)sqrtf((vec[0] * vec[0]) + (vec[1] * vec[1]) + (vec[2] * vec[2]));
	if (len < 0.00001f)
		return false;
	float koef = desired_length / len;

	for (int z = 0; z < 3; z++)
		vec[z] *= koef;

	return true;
}





void geometry::rotate3DVector(float * v, float angle, float * axis)
{
	float starting_size = sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	if ((starting_size < 0.00001f) && (starting_size > -0.00001f))
	{
		return;
	}
	if ((angle < 0.00001f) && (angle > -0.00001f))
	{
		return;
	}
	float norm_starting[3];
	for (int z = 0; z < 3; z++)
		norm_starting[z] = v[z] / starting_size;



	float rot_ax_size = sqrt(axis[0] * axis[0] + axis[1] * axis[1] + axis[2] * axis[2]);

	float vec[3];
	for (int z = 0; z < 3; z++)
		vec[z] = axis[z] / rot_ax_size;


	float c = (float)cos(angle);
	float s = (float)sin(angle);
	float t = (float)(1.0f - cos(angle));


	v[0] = norm_starting[0] * (t*vec[0] * vec[0] + c) + norm_starting[1] * (t*vec[0] *vec[1] - s * vec[2]) + norm_starting[2] * (t*vec[0]*vec[2] + s * vec[1]);
	v[1] = norm_starting[0] * (t*vec[0] * vec[1] + s * vec[2]) + norm_starting[1] * (t*vec[1] *vec[1] + c) + norm_starting[2] * (t*vec[1] *vec[2] - s * vec[0]);
	v[2] = norm_starting[0] * (t*vec[0] * vec[2] - s * vec[1]) + norm_starting[1] * (t*vec[2] * vec[1] + s * vec[0]) + norm_starting[2] * (t*vec[2] * vec[2] + c);

	for(int z = 0;z<3;z++)
		v[z] *= starting_size;


}


bool geometry::rayToSphereAligned(float * ray, float r, float * res)
{

	float a = ray[3] * ray[3] + ray[4] * ray[4] + ray[5] * ray[5];
	float b = (2.0f * ray[3] * ray[0]) + (2.0f * ray[1] * ray[4]) + (2.0f * ray[2] * ray[5]);
	float c = ray[2] * ray[2] + ray[0] * ray[0] + ray[1] * ray[1] - r * r;


	bool q_solved = quadricEquation(a, b, c, res);
	if (q_solved)
	{
		if ((res[0] < 0.0f) && (res[1] < 0.0f))
			return false;

		if ((res[0] > 0.0f) && (res[1] > 1.0f))
			return false;

		if (res[0] > res[1])
			res[0] = res[1];

		if(res[0] < 0.0f)
			res[0] = 0.0f;

		return true;

	}
	else
		return false;
}


bool geometry::quadricEquation(float a, float b, float c, float * res)
{
	// fprintf(stderr,"A=%f, B=%f, C=%f\n",a,b,c);
	if ((a == 0.0f) && (fabsf(b)<0.000001f))
		return false;

	float d = (b * b) - (4.0f * a * c);
	if (d < 0.0f)
		return false;

	d = sqrt(d);

	res[0] = (-b + d) / (2.0f * a);
	res[1] = (-b + -d) / (2.0f * a);

	return true;

}
float geometry::vectorNorm(float * a)
{
	return (float)sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
}

float geometry::multiplyVectorDot(float ax, float ay, float az, float bx, float by, float bz)
{
	return ax * bx + ay * by + az * bz;
}

float geometry::angleBetweenVectors(float * a, float * b)
{
	float norm = vectorNorm(a) *  vectorNorm(b);
	if (norm == 0.0f)
		return 0.0f;
	//fprintf(stderr,"Norm is [%f]",norm);
	float tempx = multiplyVectorDot(a[0], a[1], a[2], b[0], b[1], b[2]);
	if (tempx > norm)
		return (float)acos(1.0f);
	else if (tempx < -norm)
		return (float)acos(-1.0f);

	return (float)acos(tempx / norm);
}



void geometry::pointMatrixVectorTo(float * mat, float * ref_vec, float * look_vec)
{
	float res[3];
	multiplyVectorCross(res, ref_vec, look_vec);
	

	float angle = angleBetweenVectors(ref_vec, look_vec);

	if ((res[0] == res[1]) && (res[1] == res[2]) && (res[2] == 0.0f))//if cross product cannot be obtained angle is either 0 or 180 degrees
	{
		
		if (fabs(angle) < 0.00001f)//if angle is 0 degrees no need of rotation,
			return;
		else
		{
			multiplyVectorCross(res, ref_vec[0], ref_vec[1], ref_vec[2], look_vec[0] - 2.5f, look_vec[1] + 5.1f, look_vec[2] + 2.3f);//if angle is 180, let's just fin any vector that will be normal to ref_x, this also hast to be normal to look vector as well,
			if ((res[0] == res[1]) && (res[1] == res[2]) && (res[2] == 0.0f))//if the vector is parallel to our random choosen numbers, let's pick other numbers, it cannot be parallel to 2 different vectors
			{
				multiplyVectorCross(res, ref_vec[0], ref_vec[1], ref_vec[2], look_vec[0] + 3.5f, look_vec[1] - 1.1f, look_vec[2] + 1.0f);//we've covered all possible exceptions
			}

		}
		
	}
	rotateMatrix3D(mat, res, angle);//rotate matrix around the cross vector

}

void geometry::repointVectorTo(float * vec, float * ref_vec, float * look_vec)
{
	float res[3];
	multiplyVectorCross(res, ref_vec, look_vec);


	float angle = angleBetweenVectors(ref_vec, look_vec);

	if ((res[0] == res[1]) && (res[1] == res[2]) && (res[2] == 0.0f))//if cross product cannot be obtained angle is either 0 or 180 degrees
	{

		if (fabs(angle) < 0.00001f)//if angle is 0 degrees no need of rotation,
			return;
		else
		{
			multiplyVectorCross(res, ref_vec[0], ref_vec[1], ref_vec[2], look_vec[0] - 2.5f, look_vec[1] + 5.1f, look_vec[2] + 2.3f);//if angle is 180, let's just fin any vector that will be normal to ref_x, this also hast to be normal to look vector as well,
			if ((res[0] == res[1]) && (res[1] == res[2]) && (res[2] == 0.0f))//if the vector is parallel to our random choosen numbers, let's pick other numbers, it cannot be parallel to 2 different vectors
			{
				multiplyVectorCross(res, ref_vec[0], ref_vec[1], ref_vec[2], look_vec[0] + 3.5f, look_vec[1] - 1.1f, look_vec[2] + 1.0f);//we've covered all possible exceptions
			}

		}

	}
	rotate3DVector(vec, angle, res);
}





void geometry::rotateMatrix3D(float * m, float * axis, float ang)
{
	rotate3DVector(m, ang, axis);
	rotate3DVector(m+4, ang, axis);
	rotate3DVector(m+8, ang, axis);
}




bool geometry::invertM(float * invOut, float * m)
{

	float inv[16], det;
	int i;

	inv[0] = m[5] * m[10] * m[15] -
		m[5] * m[11] * m[14] -
		m[9] * m[6] * m[15] +
		m[9] * m[7] * m[14] +
		m[13] * m[6] * m[11] -
		m[13] * m[7] * m[10];

	inv[4] = -m[4] * m[10] * m[15] +
		m[4] * m[11] * m[14] +
		m[8] * m[6] * m[15] -
		m[8] * m[7] * m[14] -
		m[12] * m[6] * m[11] +
		m[12] * m[7] * m[10];

	inv[8] = m[4] * m[9] * m[15] -
		m[4] * m[11] * m[13] -
		m[8] * m[5] * m[15] +
		m[8] * m[7] * m[13] +
		m[12] * m[5] * m[11] -
		m[12] * m[7] * m[9];

	inv[12] = -m[4] * m[9] * m[14] +
		m[4] * m[10] * m[13] +
		m[8] * m[5] * m[14] -
		m[8] * m[6] * m[13] -
		m[12] * m[5] * m[10] +
		m[12] * m[6] * m[9];

	inv[1] = -m[1] * m[10] * m[15] +
		m[1] * m[11] * m[14] +
		m[9] * m[2] * m[15] -
		m[9] * m[3] * m[14] -
		m[13] * m[2] * m[11] +
		m[13] * m[3] * m[10];

	inv[5] = m[0] * m[10] * m[15] -
		m[0] * m[11] * m[14] -
		m[8] * m[2] * m[15] +
		m[8] * m[3] * m[14] +
		m[12] * m[2] * m[11] -
		m[12] * m[3] * m[10];

	inv[9] = -m[0] * m[9] * m[15] +
		m[0] * m[11] * m[13] +
		m[8] * m[1] * m[15] -
		m[8] * m[3] * m[13] -
		m[12] * m[1] * m[11] +
		m[12] * m[3] * m[9];

	inv[13] = m[0] * m[9] * m[14] -
		m[0] * m[10] * m[13] -
		m[8] * m[1] * m[14] +
		m[8] * m[2] * m[13] +
		m[12] * m[1] * m[10] -
		m[12] * m[2] * m[9];

	inv[2] = m[1] * m[6] * m[15] -
		m[1] * m[7] * m[14] -
		m[5] * m[2] * m[15] +
		m[5] * m[3] * m[14] +
		m[13] * m[2] * m[7] -
		m[13] * m[3] * m[6];

	inv[6] = -m[0] * m[6] * m[15] +
		m[0] * m[7] * m[14] +
		m[4] * m[2] * m[15] -
		m[4] * m[3] * m[14] -
		m[12] * m[2] * m[7] +
		m[12] * m[3] * m[6];

	inv[10] = m[0] * m[5] * m[15] -
		m[0] * m[7] * m[13] -
		m[4] * m[1] * m[15] +
		m[4] * m[3] * m[13] +
		m[12] * m[1] * m[7] -
		m[12] * m[3] * m[5];

	inv[14] = -m[0] * m[5] * m[14] +
		m[0] * m[6] * m[13] +
		m[4] * m[1] * m[14] -
		m[4] * m[2] * m[13] -
		m[12] * m[1] * m[6] +
		m[12] * m[2] * m[5];

	inv[3] = -m[1] * m[6] * m[11] +
		m[1] * m[7] * m[10] +
		m[5] * m[2] * m[11] -
		m[5] * m[3] * m[10] -
		m[9] * m[2] * m[7] +
		m[9] * m[3] * m[6];

	inv[7] = m[0] * m[6] * m[11] -
		m[0] * m[7] * m[10] -
		m[4] * m[2] * m[11] +
		m[4] * m[3] * m[10] +
		m[8] * m[2] * m[7] -
		m[8] * m[3] * m[6];

	inv[11] = -m[0] * m[5] * m[11] +
		m[0] * m[7] * m[9] +
		m[4] * m[1] * m[11] -
		m[4] * m[3] * m[9] -
		m[8] * m[1] * m[7] +
		m[8] * m[3] * m[5];

	inv[15] = m[0] * m[5] * m[10] -
		m[0] * m[6] * m[9] -
		m[4] * m[1] * m[10] +
		m[4] * m[2] * m[9] +
		m[8] * m[1] * m[6] -
		m[8] * m[2] * m[5];

	det = m[0] * inv[0] + m[1] * inv[4] + m[2] * inv[8] + m[3] * inv[12];

	if (det == 0)
		return false;

	det = 1.0f / det;


	for (int z = 0; z < 16; z++)
		invOut[z] = inv[z] * det;

	return true;
}



void geometry::multiplyMM(float * rm, float * mr, float * ml)
{


	float tmp[16];

	

	tmp[0] = ml[0] * mr[0]
		+ ml[1] * mr[4]
		+ ml[2] * mr[8]
		+ ml[3] * mr[12];

	tmp[1] = ml[0] * mr[1]
		+ ml[1] * mr[5]
		+ ml[2] * mr[9]
		+ ml[3] * mr[13];

	tmp[2] = ml[0] * mr[2]
		+ ml[1] * mr[6]
		+ ml[2] * mr[10]
		+ ml[3] * mr[14];

	tmp[3] = ml[0] * mr[3]
		+ ml[1] * mr[7]
		+ ml[2] * mr[11]
		+ ml[3] * mr[15];



	tmp[4] = ml[4] * mr[0]
		+ ml[5] * mr[4]
		+ ml[6] * mr[8]
		+ ml[7] * mr[12];

	tmp[5] = ml[4] * mr[1]
		+ ml[5] * mr[5]
		+ ml[6] * mr[9]
		+ ml[7] * mr[13];

	tmp[6] = ml[4] * mr[2]
		+ ml[5] * mr[6]
		+ ml[6] * mr[10]
		+ ml[7] * mr[14];

	tmp[7] = ml[4] * mr[3]
		+ ml[5] * mr[7]
		+ ml[6] * mr[11]
		+ ml[7] * mr[15];





	tmp[8] = ml[8] * mr[0]
		+ ml[9] * mr[4]
		+ ml[10] * mr[8]
		+ ml[11] * mr[12];

	tmp[9] = ml[8] * mr[1]
		+ ml[9] * mr[5]
		+ ml[10] * mr[9]
		+ ml[11] * mr[13];

	tmp[10] = ml[8] * mr[2]
		+ ml[9] * mr[6]
		+ ml[10] * mr[10]
		+ ml[11] * mr[14];

	tmp[11] = ml[8] * mr[3]
		+ ml[9] * mr[7]
		+ ml[10] * mr[11]
		+ ml[11] * mr[15];



	tmp[12] =
		ml[12] * mr[0]
		+ ml[13] * mr[4]
		+ ml[14] * mr[8]
		+ ml[15] * mr[12];

	tmp[13] =
		ml[12] * mr[1]
		+ ml[13] * mr[5]
		+ ml[14] * mr[9]
		+ ml[15] * mr[13];

	tmp[14] =
		ml[12] * mr[2]
		+ ml[13] * mr[6]
		+ ml[14] * mr[10]
		+ ml[15] * mr[14];

	tmp[15] =
		ml[12] * mr[3]
		+ ml[13] * mr[7]
		+ ml[14] * mr[11]
		+ ml[15] * mr[15];
	/**/

	for (int z = 0; z<16; z++)
		rm[z] = tmp[z];



}


void geometry::loadIdentity(float * matrix)
{
	for (int z = 0; z < 16; z++)
		matrix[z] = 0.0f;

	for (int z = 0; z < 4; z++)
		matrix[z * 4 + z] = 1.0f;



}


void geometry::rotateM(float * m, float angle, float x, float y, float z)
{
	float rotateMatrix[16];
	setRotateM(&rotateMatrix[0], angle, x, y, z);
	multiplyMM(&m[0], &m[0], &rotateMatrix[0]);

}



void geometry::setRotateM(float * m, float angle, float x, float y, float z)
{

	m[3] = 0;
	m[7] = 0;
	m[11] = 0;
	m[12] = 0;
	m[13] = 0;
	m[14] = 0;
	m[15] = 1;


	float s = (float)sin(angle);
	float c = (float)cos(angle);
	//fprintf(stderr,"Rotation axis is [%f,%f,%f].",x,y,z);
	//fprintf(stderr, "Cosinus is %f, Sinus is %f.\n",c ,s );
	if (1.0f == x && 0.0f == y && 0.0f == z)
	{
		//fprintf(stderr,"It's X ROTATION...\n");
		m[5] = c;   m[10] = c;
		m[6] = s;   m[9] = -s;
		m[1] = 0;   m[2] = 0;
		m[4] = 0;   m[8] = 0;
		m[0] = 1;
	}
	else if (0.0f == x && 1.0f == y && 0.0f == z)
	{
		//fprintf(stderr,"It's Y ROTATION...\n");
		m[0] = c;   m[10] = c;
		m[8] = s;   m[2] = -s;
		m[1] = 0;   m[4] = 0;
		m[6] = 0;   m[9] = 0;
		m[5] = 1;
	}
	else if (0.0f == x && 0.0f == y && 1.0f == z)
	{
		//fprintf(stderr,"It's Z ROTATION...\n");
		m[0] = c;   m[5] = c;
		m[1] = s;   m[4] = -s;
		m[2] = 0;   m[6] = 0;
		m[8] = 0;   m[9] = 0;
		m[10] = 1;
	}
	else
	{
		float len = sqrtf(x*x + y * y + z * z);
		if (1.0f != len)
		{
			float recipLen = 1.0f / len;
			x *= recipLen;
			y *= recipLen;
			z *= recipLen;
		}
		float nc = 1.0f - c;
		float xy = x * y;
		float yz = y * z;
		float zx = z * x;
		float xs = x * s;
		float ys = y * s;
		float zs = z * s;
		m[0] = x * x*nc + c;
		m[4] = xy * nc - zs;
		m[8] = zx * nc + ys;
		m[1] = xy * nc + zs;
		m[5] = y * y*nc + c;
		m[9] = yz * nc - xs;
		m[2] = zx * nc - ys;
		m[6] = yz * nc + xs;
		m[10] = z * z*nc + c;
	}
	//fprintf(stderr,"Rotation Matrix is ...\n");
	//print(m);
}